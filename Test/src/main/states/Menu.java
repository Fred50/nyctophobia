package main.states;

import main.StateMaster;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class Menu extends BasicGameState{
	
	private int id;
	
	public Menu(int id){
		this.id = id;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg)
			throws SlickException {
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g)
			throws SlickException {
		g.drawString("Testing the menu!", StateMaster.WIDTH / 2, StateMaster.HEIGHT / 2);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta)
			throws SlickException {
		
		Input input = gc.getInput();
		
		if(input.isKeyDown(Input.KEY_W)){
			sbg.enterState(1);
		}
		if(input.isKeyDown(Input.KEY_ESCAPE)){
			gc.exit();
		}
	}

	@Override
	public int getID() {
		return id;
	}

}
