package main.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Play extends BasicGameState
{
	private int id;
	private Vector2f mapPosition, playerPos;
	private Image map;
	private Image weapon_usp;
	private float cameraX = 0, cameraY = 0, mouseX = 0, mouseY = 0, diffX = 0, diffY = 0, cameraX2 = 0, cameraY2 = 0;
	private Image player;
	public int health = 100;
	public int armor = 100;
	public String healthText = "Health:";
	public String armorText = "Armor:";
	//public String messageText = "Something";
	public float healthX = 10;
	public float healthY = 10;
	
	public float armorX = 40;
	public float armorY = 10;
	
	public float healthTextX = 10;
	public float healthTextY = 10;
	
	public float armorTextX = 25;
	public float armorTextY = 10;
	
	private boolean playerIsDead = false;
	private boolean armorIsBroken = false;
	

	public Play(int id)
	{
		this.id = id;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
	{
		mapPosition = new Vector2f(0, 0);
		playerPos = new Vector2f (640, 360);
		map = new Image("res/backGround_two.png");
		
		player = new Image("res/grahamwalk.png");
		
		weapon_usp = new Image("res/USP_Menu.png");
		weapon_usp.setFilter(Image.FILTER_NEAREST);
		
		// Testing out scaling, results are decent. But it's better to make the artists scale the sprite by hand. - Farzad
		player = player.getScaledCopy(3f);
		player.setFilter(Image.FILTER_NEAREST);
		
		//weapon_usp = weapon_usp.getScaledCopy(0.4f);
		//weapon_usp.setFilter(Image.FILTER_NEAREST);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
	{
		
		g.translate(-cameraX, -cameraY);
		g.drawImage(map, mapPosition.x, mapPosition.y);
		g.drawImage(weapon_usp, 200, 300);
		g.drawImage(player, playerPos.x, playerPos.y);
		//convert and draw health
		g.setColor(Color.red);
		g.drawString(String.format("%d", health), healthX, healthY);
		g.drawString(healthText, healthTextX, healthTextY);
		//Draw armor
		g.setColor(Color.blue);
		g.drawString(String.format("%d", armor), armorX, armorY);
		g.drawString(armorText, armorTextX, armorTextY);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException
	{		
		Input input = gc.getInput(); 
		mouseX = input.getMouseX();
		mouseY = input.getMouseY();
		diffX = (gc.getWidth()/2) - mouseX;
		diffY = (gc.getHeight()/2) - mouseY;
		
		cameraX = (playerPos.x - gc.getWidth() * 0.5f) - diffX * 0.06f;
		cameraY = (playerPos.y - gc.getHeight() * 0.5f) - diffY * 0.06f;
		
		//Handling health GUI component
		healthX = cameraX + 165;
		healthY = cameraY + 10;
		
		healthTextX = cameraX + 100;
		healthTextY = cameraY + 10;
		
		//Handling armor GUI component
		armorX = cameraX + 270;
		armorY = cameraY + 10;
		
		armorTextX = cameraX + 215;
		armorTextY = cameraY + 10;
		
		Image cursor = new Image("res/crosshairs.png");
		//gc.setMouseCursor(cursor, 0, 0);
		
		
		if (input.isKeyDown(Input.KEY_A))
		{
			playerPos.x -= 3;
		}
		if (input.isKeyDown(Input.KEY_W))
		{
			playerPos.y -= 3;
		}
		if (input.isKeyDown(Input.KEY_S))
		{
			playerPos.y += 3;
		}
		if (input.isKeyDown(Input.KEY_D))
		{
			playerPos.x += 3;
		}
		if (input.isKeyDown(Input.KEY_ESCAPE))
		{
			gc.exit();
		}
		if (input.isKeyDown(Input.KEY_SPACE))
		{
			playerPos = new Vector2f (640, 360);
		}
		//This function set is just to test the Health and Armor functionality
		if (input.isKeyPressed(Input.KEY_P))
		{
			if(armor == 0)
			{
				armorIsBroken = true;
				if(armorIsBroken = true)
				{
					health -=5;
				}
			}
			else
			{
				armor -= 5;
			}
		}
		if (input.isKeyPressed(Input.KEY_H))
		{
			if(health < 100)
			{
				health += 5;
			}
			if(health == 100)
			{
				if(armor < 100)
				{
					armor +=5;
				}
			}
			else
			{
				return;
			}
			
		}
		//End function set for armor and health
		if (health == 0)
		{
			//remind me to add a timer in here.
			//messageText = "You are dead";
			//messageText = "Closing in 3...";
			//messageText = "Closing in 2...";
			//messageText = "Closing in 1...";
			System.out.println("you died");
			playerIsDead = true;
			
		}
		if (playerIsDead == true)
		{
			gc.exit();
		}
		float deltaX = mouseX - ((playerPos.x + player.getWidth()/2) - cameraX);
		float deltaY = mouseY - ((playerPos.y + player.getHeight()/2) - cameraY);
		//double direction = 360 + Math.toDegrees(Math.atan2(deltaY, deltaX)) % 360;
		player.setRotation((float) angleOfLineDeg(deltaX, deltaY));
		//System.out.println((int)(playerPos.x - cameraX) + ", " + (int)(playerPos.y - cameraY));
	}
	
	public static float angleOfLineDeg(float dX, float dY) {


		if ((dY == 0) && dX > 0) {
			return (float) -90.0;
		}

		if (dY == 0 && dX < 0) {
			return (float) -270.0;
		}

		if (dX > 0 && dY > 0) {
			return (float) -(Math.toDegrees(Math.atan(dX / dY)));
		}

		if (dX < 0 && dY > 0) {
			return (float) -(360 + Math.toDegrees(Math.atan(dX / dY)));
		}

		if (dY < 0) {
			return (float) -(180 + Math.toDegrees(Math.atan(dX / dY)));
		} else {
			return (float) 0.0;
		}

}

	@Override
	public int getID()
	{
		return id;
	}
}
