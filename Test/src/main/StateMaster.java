package main;

import main.states.Menu;
import main.states.Play;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class StateMaster extends StateBasedGame
{
	
	public static int WIDTH = 1280;
	public static int HEIGHT = 720;
	
	//Booleans
	public static boolean DEBUG = true;

	public StateMaster(String name)
	{
		super(name);
		this.addState(new Menu(0));
		this.addState(new Play(1));
	}

	public static void main(String[] args)
	{
		AppGameContainer agc;
		try
		{
			agc = new AppGameContainer(new StateMaster("Test Game For Nyctophobia"));
			agc.setDisplayMode(WIDTH, HEIGHT, false);
			agc.setFullscreen(true);
			agc.setTargetFrameRate(60);
			agc.setShowFPS(DEBUG);
			agc.setVSync(DEBUG);
			agc.setSmoothDeltas(true);
			agc.start();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException
	{
		this.getState(0).init(gc, this);
	}

}
