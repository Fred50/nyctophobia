package main;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class StateMaster extends StateBasedGame
{

	public StateMaster(String name)
	{
		super(name);
		this.addState(new StatePlay(0));
	}

	public static void main(String[] args)
	{
		AppGameContainer agc;
		try
		{
			agc = new AppGameContainer(new StateMaster("Test Game For Nyctophobia"));
			agc.setDisplayMode(1280, 720, false);
			agc.setTargetFrameRate(59);
			agc.setSmoothDeltas(true);
			agc.start();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException
	{
		this.getState(0).init(gc, this);
	}

}
